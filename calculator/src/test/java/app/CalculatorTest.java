package app;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class CalculatorTest
{
    static Calculator calculator;

    @BeforeAll
    public static void setup()
    {
        calculator = new Calculator();
    }
    @Test
    public void test_addNumbers()
    {
        Assertions.assertEquals(5, calculator.add(2, 3));
    }
}

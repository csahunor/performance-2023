package app;

import domain.Addition;

public class Calculator
{
    public double add(double a, double b)
    {
        return new Addition(a, b).execute();
    }

    public double subtract(double a, double b)
    {
        return 0;
    }

    public double multiply(double a, double b)
    {
        return 0;
    }

    public double divide(double a, double b)
    {
        return 0;
    }
}
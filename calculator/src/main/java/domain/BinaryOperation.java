package domain;

public abstract class BinaryOperation implements Operation
{
    protected BinaryOperation(double firstTerm, double secondTerm)
    {
        this.firstTerm = firstTerm;
        this.secondTerm = secondTerm;
    }
    protected double firstTerm;
    protected double secondTerm;
}

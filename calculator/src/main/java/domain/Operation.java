package domain;

public interface Operation
{
    double execute();
}

package domain;


public class Addition extends BinaryOperation
{
    public Addition(double firstTerm, double secondTerm)
    {
        super(firstTerm, secondTerm);
    }

    @Override
    public double execute()
    {
        return firstTerm + secondTerm;
    }
}
